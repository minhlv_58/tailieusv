<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if ( ! function_exists('headerData()'))
 {
   function headerData()
   {
   		$data = array();
    	if(func_num_args()>0 && is_array(func_get_args(0))){
    		$data = &func_get_args(0);
    	}
    	$CI = &get_instance();
    	$CI->load->model('school');
    	$CI->load->model('group');
    	$CI->load->model('major');  	
    	$data['hdr_schools'] = $CI->school->findAll();
    	$data['hdr_majors'] = $CI->major->findAll();
    	$data['hdr_groups'] = $CI->group->findAll();
    	return $data;
   }
 }