<?php 
/*
standar flash instance:
{
	'type': 'default' | 'success' | 'primary' | 'danger' | 'info' | warning;
	'icon': 'one of bootstrap glyion' or false;
	'title': 'title mesage' or false;
	'content': 'content mesage' false;
}
*/
?>

<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if ( ! function_exists('notice_render()'))
 {
   function notice_render()
   {
      $CI = &get_instance();
      $data['notices'] = $CI->session->flashdata('notice');
      $CI->load->view('notice', $data);
   }
 }

 if ( ! function_exists('new_notice()'))
 {
 	function new_notice($option){
 		$flash = array(
 			'type' => 'default',
 			'icon' => false,
 			'title' => false,
 			'content' => false
 			);
 		foreach ($flash as $key => $value) {
			$flash[$key] = $option[$key]; 			
 		}

 		$CI = &get_instance();
 		$notices = $CI->session->flashdata('notice');

 		if($notices == false){
 			$notices = array();
 		}
 		array_push($notices, $flash);
 		$CI->session->set_flashdata('notice', $notices);
 	}
 }