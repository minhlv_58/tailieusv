<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {
	public function index()
	{
		$this->load->helper('header');
		$data = headerData();
		$data['title'] = 'Home';
		$this->load->model('user');
		$newUser = $this->user->findLastUser();
		$totalUser = $this->user->totalUser();
		$data['newUser'] = $newUser[0]->user_name;
		$data['totalUser'] = $totalUser;
		$this->load->view('personal', $data);
	//	$this->output->cache(1);
	}
	public function uet(){
		echo 'uet';
	}
}
