<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$this->load->helper('header');
		$data = headerData();
		$data['title'] = 'Home';
		$this->load->model('user');
		$this->load->model('document');
		$hotDocs = $this->document->findHotDocument();
		$newUser = $this->user->findLastUser();
		$totalUser = $this->user->totalUser();
		$totalDoc = $this->document->totalDoc();
		$newDoc = $this->document->findLastDoc();
		$data['newUser'] = $newUser[0]->user_name;
		$data['totalUser'] = $totalUser;
		$data['hotDocs'] = $hotDocs;
		$data['totalDoc'] = $totalDoc;
		$data['newDoc'] = $newDoc[0];
		$this->load->view('home', $data);
	//	$this->output->cache(1);
	}
	public function uet(){
		echo 'uet';
	}
}
