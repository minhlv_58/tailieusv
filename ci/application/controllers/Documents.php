<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends CI_Controller {
	public function index($id)
	{
		$this->load->helper('header');
		$data = headerData();
		$this->load->model('document');
		$this->load->model('user');
		$doc = $this->document->findById($id);	
		if($doc) {
			$data['doc'] = $doc[0];
			$data['title'] = $doc[0]->doc_tittle.' - Docs ';
			if($doc[0]->doc_up_by){
				$user_upload = $this->user->findById($doc[0]->doc_up_by);
				if($user_upload) $data['doc_up_by'] = $user_upload[0];
				else $data['doc_up_by'] = null;
			}
			else $data['doc_up_by'] = null;
		}
		else {
			$data['doc'] =null;
			$data['title'] = 'Không tồn tại tài liệu';
		}
		$this->load->view('documents', $data);
	//	$this->output->cache(1);
	}
	public function uet(){
		echo 'uet';
	}
}
