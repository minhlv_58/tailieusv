<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schools extends CI_Controller {
	public function index($university)
	{
		$university = strtoupper($university);
		$school= array("UET","ULIS","UEB");
		if (!in_array($university, $school))
		  {
		  	redirect('#', 'refresh');
		  }
		$this->load->helper('header');
		$data = headerData();
		$this->load->model('school');
		$sch = $this->school->find(array('school_name_short'=>$university),1,0);
		$data['user_school'] = $sch[0];
		$this->load->model('faculty');
		$fac = $this->faculty->findBy(array('faculty_school'=>$sch[0]->school_id));
		$data['faculties'] = $fac;
		$data['title'] = $university;
		$this->load->model('subject');
		$subByFac = array();
		foreach ($fac as $fa) {
			$subs = $this->subject->subjectByFaculty($fa->faculty_id);
		 	$subByFac = array_merge($subByFac, array('fa'.$fa->faculty_id => $subs));
		} 
		$data['subjectByFaculty'] = $subByFac;
		$this->load->view('schools', $data);
	}
}
