<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function authenticate(){
		$this->authenticate->lookup();
		echo json_encode($this->session);
	}
	public function login()
	{
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->load->helper('url');
			return $this->verify();
		};
		$this->load->helper('header');
		$data = headerData();
		$data['title'] = 'Login';
		$data['csrf'] = array(
        	'name' => $this->security->get_csrf_token_name(),
        	'hash' => $this->security->get_csrf_hash()
		);
		$this->load->view('login', $data);
		//	$this->output->cache(1);
	}

	public function logout()
	{
		$this->authenticate->logout();
		redirect('/auth/login', 'refresh');
	}

	public function verify(){
		if($this->input->server('REQUEST_METHOD') != 'POST'){
			$this->load->helper('url');
			redirect('/auth/login', 'refresh');
		};

		$data = $this->security->xss_clean($_POST);
		$error = false;
		if(!isset($_POST['email'])){
			$nt = $arrayName = array(
				'type' => 'danger',
				'icon' => 'warning-sign',
				'title' => false,
				'content' => 'bạn chưa điền email' 
				);
			new_notice($nt);
			$error = true;
		}

		if(!isset($_POST['password'])){
			$nt = $arrayName = array(
				'type' => 'danger',
				'icon' => 'warning-sign',
				'title' => false,
				'content' => 'bạn chưa điền mật khẩu' 
				);
			new_notice($nt);
			$error = true;
		}
		$user = $this->authenticate->user_exits($data['email']);
		if(!$user){
			$nt = $arrayName = array(
				'type' => 'success',
				'icon' => 'warning-sign',
				'title' => false,
				'content' => 'ngưòi dùng không tồn tại' 
				);
			new_notice($nt);
			$error = true;
		} else {
			if($this->authenticate->verify($user, $_POST['password'])){
				$this->authenticate->login($user);
			} else {
				$nt = $arrayName = array(
				'type' => 'danger',
				'icon' => 'warning-sign',
				'title' => false,
				'content' => 'sai mật khẩu' 
				);
			new_notice($nt);
			$error = true;
			}
		}
		if($error){
		//	echo json_encode($this->session);
			$this->session->keep_flashdata('notice');
			redirect('/auth/login', 'refresh');
		} else {
			redirect('/home/index', 'refresh');
		}
	}
}
