<?php /*

    1. When the user successfully logs in with Remember Me checked, a login cookie is
    issued in addition to the standard session management cookie.
    2. The login cookie contains a series identifier and a token. The series and token
    are unguessable random numbers from a suitably large space. Both are stored together
    in a database table, the token is hashed (sha256 is fine).
    3. When a non-logged-in user visits the site and presents a login cookie, the series
    identifier is looked up in the database.
        3.1. If the series identifier is present and the hash of the token matches the
        hash for that series identifier, the user is considered authenticated. A new token
        is generated, a new hash for the token is stored over the old record, and a new login
        cookie is issued to the user (it's okay to re-use the series identifier).
        3.2 If the series is present but the token does not match, a theft is assumed. The user
        receives a strongly worded warning and all of the user's remembered sessions are deleted.
        3.3 If the username and series are not present, the login cookie is ignored.

*/ ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate {
		public function __construct(){
			$this->is_login();
		}
        public function user_exits($email){
        	//return false or user
        	$CI =  &get_instance();
        	$CI->load->model('user');
        	$user = $CI->user->find(array('user_email' => $email), 1, 0);
        	if($user){
                return $user[0];    
            }
            return false;
        }

        public function verify($user, $pass){
            //test no salt
        	//$salt = $user->user_salt;
            //$password = password_hash($salt.$pass, PASSWORD_DEFAULT);
            //return password_verify ($password , $user_pass);
            return $user->user_pass == $pass;
        }
        public function lookup(){
        }
        public function login($usr){
        	$CI =  &get_instance();
        	$CI->load->library('session');
        	$CI->load->helper('string');
        	$session_tok = random_string('alnum', 16);
        	$newdata = array();
        	$newdata['session_tok'] = $session_tok;
        	$newdata['user_id'] = 'minhlv';
			$CI->session->set_userdata('logged_in',$newdata);

        	$tok = array(
		    'name'   => 'session_tok',
		    'value'  => $session_tok,
		    'expire' => '7200',
		    'domain' => 'localhost',
		    'path'   => '/',
		    'secure' => FALSE
			);

			$CI->input->set_cookie($tok);
        }

        public function logout(){
        	$CI =  &get_instance();
        	$CI->session->unset_userdata('logged_in');
        }

        public function is_login(){
        	//return tru or false
        	$CI =  &get_instance();
        	$CI->load->library('session');
        	if($CI->session->userdata('logged_in')){
        		return true;
        	}
        	return false;
        }
}