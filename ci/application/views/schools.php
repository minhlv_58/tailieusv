<?php include 'template_begin.php' ?>
<?php include 'header.php'; ?>
<main class="">
    <div class="container">
        <div class="isotope js-isotope">
            <div class="item hidden-xs- hidden-sm">
                <div class="logoHolder itemHolder">
                    <a href="#"><img src="<?php echo base_url(); if($user_school->school_logo) echo $user_school->school_white_logo; else echo'assets\img\site\logo.png';?>" alt="<?php if($user_school->school_name) echo $user_school->school_name; else echo'Chưa có tên trường!';?>"></a>
                </div>
            </div>
            <style>
                .item.careers>.imgHolder>a>.imgLink {
                    background-image: url('img/home/careers.png');
                }
                @media screen and (max-width: 767px) {
                    .item.careers>.imgHolder>a>.imgLink {
                        background-image: url('img/home/careers-square.png');
                    }
                }
            </style>
            <div class="item width3">
                <div class="itemHolder textHolder">
                    <h2 class="mb15 fwb cf1 fs32"><i class="fa fa-map-signs"></i>&nbsp;<?php if($user_school->school_name) echo $user_school->school_name; else echo'Chưa có tên trường!';?></h2>
                    <p>
                        <?php if($user_school->school_desc) echo $user_school->school_desc; else echo'Chưa có mô tả!';?></p>
                </div>
            </div>

            <div class="item">
                <div class="itemHolder textHolder">
                    <h2 class="mb15 fwb cf1 fs32"><i class="fa fa-paper-plane"></i>&nbsp;Thư Viện Phong Phú</h2>
                    <p>Thư viện đề thi, tài liệu chuyên ngànhm sách, khóa luận phong phú đầy đủ và cập nhật không ngừng.</p>
                </div>
            </div>
            <div class="item">
                <div class="itemHolder textHolder">
                    <h3 class="mb15 fwb cf1 fs32"><i class="fa fa-cloud"></i>&nbsp;Tài Liệu &nbsp; Của Tôi</h3>
                    <p>Cung cập cho bạn một kho tài liệu cá nhân online để bạn có thể đọc, chia sẻ và tải xuống mọi lúc mọi nơi.</p>
                </div>
            </div>
            <div class="item">
                <div class="itemHolder textHolder">
                    <h2 class="mb15 fwb cf1 fs32">
                        <i class="fa fa-crosshairs"></i>&nbsp;các trường thành viên
                        </h2>
                    <p>với mỗi trường thành viên chúng toi cung cập một trang tài liệu riêng, ngoài ra còn có các thông tin hỗ trợ học tập khác.</p>
                </div>
            </div>
            <div class="item">
                <div class="itemHolder textHolder">
                    <h2 class="mb15 fwb cf1 fs32">
                        <i class="fa fa-eye"></i>&nbsp;hoàn toàn miễn phí
                        </h2>
                    <p>tất cả các tài liệu được cộng đồng chia sẻ đề hoàn toàn miễn phí. bạn chỉ cần upload lên một tài liệu để có thể download.</p>
                </div>
            </div>
            <div class="item width3 height2">
                <div class="itemHolder">
                    <div id='menu_tab' class="col-md-3">
                             <!-- required for floating -->
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills nav-stacked">
                                <?php $class = 'active' ?>  
                                <?php foreach($faculties as $fac): ?> 
                                <li class="<?php echo $class; ?>" >
                                    <a href="<?php echo '#tab'.$fac->faculty_id; ?>" data-toggle="tab"><i class="fa fa-briefcase"></i>&nbsp;&nbsp;<?php echo $fac->faculty_name; ?></a>
                                </li>
                                <?php $class = ''; ?>
                                <?php endforeach; ?>  
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <!-- Tab panes -->
                           <div class="tab-content">
                                <?php $class = 'active' ?>  
                                <?php foreach($faculties as $fac): ?> 
                                <div class="tab-pane <?php echo $class; ?>" id="<?php echo 'tab'.$fac->faculty_id; ?>">
                                    <?php $subs = $subjectByFaculty['fa'.$fac->faculty_id]; ?>
                                    <?php if(!empty($subs)): ?>
                                    <?php foreach($subs as $sub): ?>
                                        <a href="<?php echo '#'; ?>"><?php echo $sub->subject_name; ?></a></br>
                                     <?php $class = ''; ?>    
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                    <span>không có môn học nào</span>
                                    <?php endif; ?>    
                                </div>
                                  <?php endforeach; ?>   
                            </div>
                        </div>  
                </div>
            </div>
            <div class="item careers">
                <div class="itemHolder textHolder">
                    <h3 class="mb15 fwb cf1 fs20">Thống kê</h3>
                </div>
            </div>
            <div class="item">
                <div class="itemHolder textHolder">
                    <h3 class="mb15 fwb cf1 fs20">Thống kê</h3>
                </div>
            </div>
            <div class="item width4" style="height: 70px">
                <div class="itemHolder" style="height: 60px; padding: 0.5%">
                    <h3 class="mb15 fwb cf1 fs30">Các Tài Liệu Nổi bật</h3>
                </div>
            </div>
            <div class="item lab">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="home/unique-culture">
                        <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                        <h3 class="cf3 fs17">Our Unique Culture</h3>
                    </a>
                </div>
            </div>
            <div class="item lab">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="home/awesome-office.html">
                        <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                        <h3 class="cf3 fs17">Our Awesome Office</h3>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="events">
                        <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                        <h3 class="cf3 fs17">Discover our great events & Join with us!</h3>
                    </a>
                </div>
            </div>
            <div class="item lab">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="home/best-moments.html">
                        <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                        <h3 class="cf3 fs17">The best moments of SSS</h3>
                    </a>
                </div>
            </div>
            <div class="item lab">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="home/on-media.html">
                        <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                        <h3 class="cf3 fs17">On Media</h3>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="http://bit.ly/7corevalues">
                        <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                        <h3 class="cf3 fs17">SSS 7 core values</h3>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="http://bit.ly/7corevalues">
                        <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                        <h3 class="cf3 fs17">SSS 7 core values</h3>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="http://bit.ly/7corevalues">
                        <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                        <h3 class="cf3 fs17">SSS 7 core values</h3>
                    </a>
                </div>
            </div>
            <div class="item width4" style="height: 70px">
                <div class="itemHolder" style="height: 60px; padding: 0.5%">
                    <h3 class="mb15 fwb cf1 fs30">Sách Hay Trong ngày</h3>
                </div>
            </div>
            <div class="item lab">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="home/unique-culture">
                        <div class="fullS imgLink" style="background-image:url('img/home/book.jpg')"></div>
                        <h3 class="cf3 fs17">Our Unique Culture</h3>
                    </a>
                </div>
            </div>
            <div class="item lab">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="home/awesome-office.html">
                        <div class="fullS imgLink" style="background-image:url('img/home/book.jpg')"></div>
                        <h3 class="cf3 fs17">Our Awesome Office</h3>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="events">
                        <div class="fullS imgLink" style="background-image:url('img/home/book.jpg')"></div>
                        <h3 class="cf3 fs17">Discover our great events & Join with us!</h3>
                    </a>
                </div>
            </div>
            <div class="item lab">
                <div class="itemHolder imgHolder fancyHover">
                    <a href="home/best-moments.html">
                        <div class="fullS imgLink" style="background-image:url('img/home/book.jpg')"></div>
                        <h3 class="cf3 fs17">The best moments of SSS</h3>
                    </a>
                </div>
            </div>

</main>
<?php include 'footer.php' ?>
<?php include 'template_end.php' ?>