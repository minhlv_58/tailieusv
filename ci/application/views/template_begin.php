<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<!-- Mirrored from www.siliconstraits.vn/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 08 Jan 2016 22:39:23 GMT -->

<head>
    <title><?php if(isset($title)) echo $title.' -'; ?> Tài Liệu Sinh Viên</title>
    <link rel="icon" type="image/png" href="<?php echo asset_url(); ?>favicon-32x32.png" sizes="32x32" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link href='<?php echo asset_url(); ?>css/css99a0.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo asset_url(); ?>css/custom039c.css?ver=3.6" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo asset_url(); ?>css/bootstrapValidator.min039c.css?ver=3.6" />
    <link href="<?php echo asset_url(); ?>fonts/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/mfizz/font-mfizz.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/jquery.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/isotope.pkgd.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/main039c.js?ver=3.6"></script>
</head>

<body>
    <!--[if lt IE 7]>
          <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
      <![endif]-->
    