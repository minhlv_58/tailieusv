<header>
        <div class="container">
            <nav>
                <a href="<?php echo base_url().'index.php';?>" class="logo" data-filter="*"><i class="ico icoLogoWhite"></i></a>
                <ul class="navMain visible-md visible-lg">
                    <li>
                        <a href="#" data-filter=".team, .contact"><i class="ico icoGroupGray"></i></a>
                        <span>Trường</span>
                        <div class="row submenu-wrap">
                            <div class="col-md-12 submenu" id="school-sub" style="width: 800px;">
                                <ul>
                                    <?php foreach($hdr_schools as $school):?>
                                        <li><a href="<?php echo base_url().'index.php/'.$school->school_name_short; ?>"><?php echo $school->school_name; ?></a></li>
                                    <?php endforeach; ?>    
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#" data-filter=".lab, .contact"><i class="ico icoBulbGray"></i></a>
                        <span>Ngành</span>
                        <div class="row submenu-wrap">
                            <div class="col-md-4 submenu">
                            <div class="submenu list-sub">
                                <ul>
                                    <?php foreach($hdr_groups as $group): ?>
                                        <li><a href="#">Nhóm <?php echo $group->group_name; ?></a>
                                        <ul class="submenu2">
                                            <?php foreach($hdr_majors as $major): ?>
                                                <?php if($major->major_group == $group->group_id): ?>
                                                    <li><a href="#"><?php echo $major->major_name; ?></a></li> 
                                                <?php endif; ?>    
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>    
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#" data-filter=".blog, .contact"><i class="ico icoPenGray"></i></a>
                        <span>Danh Mục</span>
                        <div class="submenu list-sub">
                            <ul>
                                <li><a href="#">Tài Liệu</a></li>
                                <li><a href="#">Đề Thi</a></li>
                                <li><a href="#">Bài Tập</a></li>
                                <li><a href="#">Sách</a></li>
                                <li><a href="#">Giáo Viên</a></li>
                                <li><a href="#">Khác</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#" data-filter=".events, .contact"><i class="ico icoEventGray"></i></a>
                        <span>Cá Nhân</span>
                    </li>
                    <li>
                        <a href="#" data-filter=".careers, .contact"><i class="ico icoCareerGray"></i></a>
                        <span>Thông Tin</span>
                    </li>
                    <li>
                        <a href="#" data-filter=".airCoffee, .contact"><i class="ico icoCupCfGray"></i></a>
                        <span class="longText">Liên Hệ</span>
                    </li>
                </ul>
                <a href="<?php echo base_url().'index.php'.'/login';?>" class="  navContact visible-md visible-lg"><i class="ico icoBookGray"></i>
                    <span>Đăng nhập</span>
                </a>
                <div class="dropdown pull-right visible-xs visible-sm">
                    <button class="btn btn-default btn-lg dropdown-toggle ma5" type="button" id="dropdownMenu1" data-toggle="dropdown">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right smack-animation dropdownMenu1" role="menu" aria-labelledby="dropdownMenu1">
                        <div class="row top">
                            <div class="col-xs-6">
                                <ul class="multi-column-dropdown">
                                    <li role="presentation">
                                        <a role="menuitem" href="team" data-filter=".team, .contact">
                                            <div class="ico-holder">
                                                <i class="ico icoGroupGray"></i>
                                            </div>
                                            <span class="longText">Team</span>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" href="http://blog.siliconstraits.vn/" data-filter=".blog, .contact">
                                            <div class="ico-holder"><i class="ico icoPenGray"></i></div>
                                            <span class="longText">Blog</span>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" href="events" class="">
                                            <div class="ico-holder"><i class="ico icoEventGray"></i></div>
                                            <span class="longText">Events</span>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" href="contact" class="">
                                            <div class="ico-holder"><i class="ico icoBookGray"></i></div>
                                            <span class="longText">Contact</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-6">
                                <ul class="multi-column-dropdown">
                                    <li role="presentation">
                                        <a role="menuitem" href="labs" data-filter=".lab, .contact">
                                            <div class="ico-holder"><i class="ico icoBulbGray"></i></div>
                                            <span class="longText">Labs</span>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" href="careers" data-filter=".airCoffee, .contact">
                                            <div class="ico-holder">
                                                <i class="ico icoCareerGray"></i>
                                            </div>
                                            <span class="longText">Careers</span>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" href="air-lounge" data-filter=".airCoffee, .contact">
                                            <div class="ico-holder">
                                                <i class="ico icoCupCfGray"></i>
                                            </div>
                                            <span class="longText">Air Lounge</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </ul>
                </div>
            </nav>
        </div>
    </header>