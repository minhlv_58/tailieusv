<?php include 'template_begin.php' ?>
    <?php include 'header.php'; ?>  
    <main class="">
        <div class="container">
        <?php notice_render(); ?> 
            <div class="isotope js-isotope">
                <div class="item hidden-xs- hidden-sm">
                        <div class="itemHolder mixedLogoHolder">
                            <img src="<?php echo asset_url(); ?>img/login/login_ico3.png" alt="login" />
                            <h3 class="mb15 fwb fs32">Đăng nhập</h3>
                            <a href="site_register.html">Tôi chưa có tài khoản</a>
                        </div>
                </div>
                <style>
                .item.careers>.imgHolder>a>.imgLink {
                    background-image: url('img/login/login_ico.png');
                }
                
                @media screen and (max-width: 767px) {
                    .item.careers>.imgHolder>a>.imgLink {
                        background-image: url('img/login/login_ico.png');
                    }
                }
                </style>
                <div class="item width3">
                    <div class="itemHolder">
                    <div class="row" style="margin:0;padding:0">
                        <div class="col-md-7">
                		<div style="background-color:#666666; margin:0 -9px 0 -9px; padding:10px; height45:0px; color:#fff">Đăng nhập bằng tài khoản TailieuSV</div>
                            <form name="form" id="form" action="<?php echo base_url('index.php/auth/verify');?>" class="form-horizontal" method="POST" style="margin:8%">
                                <div class="input-group" style="padding:5px">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user" style="color:#fff"></i></span>
                                    <input id="user" type="email" class="form-control" name="email" value="" placeholder="Email" required style="border:solid 1px #ccc; border-radius:0 2px 2px 0; height:35px">
                                </div>
                                <div class="input-group" style="padding:5px">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock" style="color:#fff"></i></span>
                                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required style="border:solid 1px #ccc; border-radius:0 2px 2px 0; height:35px">
                                </div>
                                <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>">
                                <!-- Button -->
                                <a href="#">Quên mật khẩu</a>
                                <button type="submit" style="height:40px; border-radius:0; margin:5px" href="#" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-log-in"></i> Log in</button>
                                </form>
                            </form>
                        </div>
                        <div class="col-md-5" style="border-left: solid #ccc 2px; margin-top:5px">
                            <div style="background-color:#405d9b; height: 90px; padding:5%; color:#fff"><i class="fa fa-3x fa-facebook" style="color:#fff"></i>Đăng nhập bằng Facebook</div>
                            <div style="background-color:#c54439; height: 90px; padding:5%; color:#fff"><i class="fa fa-3x fa-google-plus"style="color:#fff"></i>Đăng nhập bằng Google+</div>
                            <div style="background-color:#33ccff; height: 90px; padding:5%; color:#fff"><i class="fa fa-3x fa-twitter"style="color:#fff"></i>Đăng nhập bằng Twitter</div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder">
                        <a href="http://www.siliconstraits.com/about">
                            <div class="fullS imgLink" style="background-image:url('img/site/upload.jpg')">
                                <h3 class="cf3 fs17">Hướng Dẫn upload Tài liệu</h3></a>
                        </div>
                        </a>
                    </div>
                </div>
                <div id="careers-banner" class="item width3" style="position: absolute; left: 0px; top: 0px;">
                    <div class="itemHolder imgHolder" style="border:0;background:#EEE;overflow:hidden">
                        <div class="fullS imgLink"></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'footer.php' ?>
<?php include 'template_end.php' ?>
