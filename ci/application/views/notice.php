<?php if(!isset($notices)){return '';} ?>

<?php foreach($notices as  $notice): ?>
<div class="notice fs16">
    <div class="alert alert-<?php echo $notice['type']; ?> fade in">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <span class=" glyphicon glyphicon-<?php echo $notice['icon']; ?>
        <?php if($notice['icon'] == false){ echo 'hidden'; } ?>"
        aria-hidden="true"></span>&nbsp;
        <strong class="<?php if($notice['title'] == false){ echo 'hidden'; } ?>"><?php echo $notice['title']; ?></strong> 
        <?php
          if ($notice['content']) {
            echo $notice['content'];
          }
        ?>
    </div>
<div>
<?php endforeach; ?>