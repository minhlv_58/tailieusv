<?php include 'template_begin.php' ?>
    <?php include 'header.php'; ?>
    <main>
        <style type="text/css">
        .docview {
            width: 100%;
            height: 99%;
        }
        .docview iframe{
            width: 100%;
            height: 100%;
        }
        </style>
        <div class="container">
            <div class="isotope js-isotope">
                <div class="item">
                    <a href="#">
                        <div class="itemHolder mixedLogoHolder">
                            <img src="<?php echo base_url().'assets\img\docview\logo_doc.png';?>" alt="tai lieu" />
                            <h3 class="mb15 fwb fs32">Tài Liệu</h3>
                        </div>
                    </a>
                </div>
                <div class="item width3">
                    <div class="itemHolder textHolder">
                        <h2 class="mb15 fwb cf1 fs32"><i class="fa fa-newspaper-o"></i>&nbsp;<?php if($doc) echo $doc->doc_tittle." (".$doc->doc_author.")"; else echo'Chưa có tên tài liệu!';?></h2>
                        <div class="info">
                            <i class="fa fa-cloud-upload">&nbsp;</i><?php if($doc_up_by) echo "<a href='#'>".$doc_up_by->user_name."</a>";?>
                            &nbsp;<i class="fa fa-calendar">&nbsp;</i><?php if($doc) echo "8/3/2016";?>
                            &nbsp;<i class="fa fa-eye">&nbsp;</i><?php if($doc) echo $doc->doc_view_total;?>
                            &nbsp;<i class="fa fa-btc">&nbsp;</i><?php if($doc->doc_cost) echo "<b>".$doc->doc_cost." point</b>"; else echo "<b>FREE</b>";?>
                        </div>
                        <div class="desc">
                            <b>Mô tả:</b>&nbsp; <?php if($doc) echo $doc->doc_desc; else echo'Chưa có mô tả!';?>
                        </div>
                        <div class="tags">
                            <span>tag 1</span>
                            <span>tag 2</span>
                            <span>tag 3</span>
                            <span>tag 4</span>
                            <span>tag 5</span>
                        </div>
                    </div>
                </div>
                <div class="item height2 lab">
                    <div class="itemHolder imgHolder">
                        thanh tác vụ
                    </div>
                </div>
                <div class="item width3 height2">
                    <div class="itemHolder">
                        <div class="docview">
                            <iframe src="http://docview.dlib.vn/tailieu/2016/20160313/tamtranduongtam/bai04_quan_ly_tien_trinh_0285.pdf?rand=936588">
                            </iframe>
                        </div>
                    </div>
                </div>
                <div class="item">
                        <div class="itemHolder">
                            ??????????
                        </div>
                </div>
                <div class="item width3">
                    <div class="itemHolder">
                        <div class="title fwb fs25 mb15">dữ liệu text</div>
                        
                    </div>
                </div>
                <div class="item width4" style="height: 70px">
                    <div class="itemHolder" style="height: 60px; padding: 0.5%">
                        <h3 class="mb15 fwb cf1 fs30">Các Tài Liệu Liên quan</h3>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/unique-culture">
                            <div class="fullS imgLink" style="background-image:url('img/docview/subjects.jpg')"></div>
                            <h3 class="cf3 fs17">các tài liệu môn ABC</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/awesome-office.html">
                            <div class="fullS imgLink" style="background-image:url('img/docview/folder.png')"></div>
                            <h3 class="cf3 fs17">các tài liệu ngành xyz</h3></a>
                    </div>
                </div>
                <div class="item">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="events">
                            <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17">Discover our great events & Join with us!</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/best-moments.html">
                            <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17">The best moments of SSS</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/unique-culture">
                            <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17">Our Unique Culture</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/awesome-office.html">
                            <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17">Our Awesome Office</h3></a>
                    </div>
                </div>
                <div class="item">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="events">
                            <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17">Discover our great events & Join with us!</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/best-moments.html">
                            <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17">The best moments of SSS</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/unique-culture">
                            <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17">Our Unique Culture</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/awesome-office.html">
                            <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17">Our Awesome Office</h3></a>
                    </div>
                </div>
                <div class="item">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="events">
                            <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17">Discover our great events & Join with us!</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/best-moments.html">
                            <div class="fullS imgLink" style="background-image:url('img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17">The best moments of SSS</h3></a>
                    </div>
                </div>
            </div>
            <!--/íotope -->
        </div>
        <!--/container-->
    </main>
    <?php include 'footer.php' ?>
<?php include 'template_end.php' ?>
