<?php include 'template_begin.php' ?>
    <?php include 'header.php'; ?>
    <main class="">
        <div class="container">
        <?php notice_render(); ?> 
            <div class="isotope js-isotope">
                <div class="item hidden-xs- hidden-sm">
                    <div class="logoHolder itemHolder">
                        <a href="#"><img src="<?php echo asset_url(); ?>/img/site/logo.png" alt="tailieusv"></a>
                    </div>
                </div>
                <style>
                .item.careers>.imgHolder>a>.imgLink {
                    background-image: url('<?php echo asset_url(); ?>/img/home/careers.png');
                }
                
                @media screen and (max-width: 767px) {
                    .item.careers>.imgHolder>a>.imgLink {
                        background-image: url('<?php echo asset_url(); ?>/img/home/careers-square.png');
                    }
                }
                </style>
                <div class="item width2 height2 hidden-xs hidden-sm">
                    <div class="itemHolder imgHolder ">
                        <div class="fullS imgLink" style="background-image:url('<?php echo asset_url(); ?>/img/home/SSS%20team.jpg')">
                        </div>
                    </div>
                </div>
                <div class="item height2">
                    <div class="itemHolder textHolder introHolder">
                        <h2 class="mb15 fwb cf1 fs32">Quick Links</h2>
                        <ul class="socialLinks clearfix">
                            <li>
                                <div class="icoHolder">
                                    <i class="ico icoWeb"></i>
                                </div>
                                <a href="http://blog.siliconstraits.vn/" target="_blank">&nbsp;&nbsp;assssssssssssssssss</a>
                            </li>
                            <li>
                                <div class="icoHolder">
                                    <i class="ico icoFb"></i>
                                </div>
                                <a href="https://www.facebook.com/siliconstraitssaigon" target="_blank">&nbsp;&nbsp;Khối kỹ thuật (40)</a>
                            </li>
                            <li>
                                <div class="icoHolder">
                                    <i class="ico icoGg"></i>
                                </div>
                                <a href="https://plus.google.com/105201677727584214411" target="_blank">&nbsp;&nbsp;Khối tự nhiên (40)</a>
                            </li>
                            <li>
                                <div class="icoHolder">
                                    <i class="ico icoIn"></i>
                                </div>
                                <a href="http://www.linkedin.com/company/3284661" target="_blank">&nbsp;&nbsp;Khối xã hội (40)</a>
                            </li>
                            <li>
                                <div class="icoHolder">
                                    <i class="ico icoTw"></i>
                                </div>
                                <a href="https://twitter.com/sssaigon" target="_blank">&nbsp;&nbsp;Khối Ngoại ngữ (40)</a>
                            </li>
                            <li>
                                <div class="icoHolder">
                                    <i class="ico icoBe"></i>
                                </div>
                                <a href="https://www.behance.net/siliconstraitssaigon" target="_blank">&nbsp;&nbsp;Khối Sư phạm (40)</a>
                            </li>
                            <li>
                                <div class="icoHolder">
                                    <i class="ico icoPlay"></i>
                                </div>
                                <a href="https://www.youtube.com/user/siliconstraitsSaigon" target="_blank">&nbsp;&nbsp;Khối Y Dược (40)</a>
                            </li>
                            <li>
                                <div class="icoHolder">
                                    <i class="ico icoPin"></i>
                                </div>
                                <a href="http://www.pinterest.com/siliconstraitss/" target="_blank">&nbsp;&nbsp;Khối Nghệ thuật (40)</a>
                            </li>
                            <li>
                                <div class="icoHolder">
                                    <i class="ico icoPin"></i>
                                </div>
                                <a href="http://www.pinterest.com/siliconstraitss/" target="_blank">&nbsp;&nbsp;Khối Nhân văn (40)</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder">
                        <a href="http://www.siliconstraits.com/about">
                            <div class="fullS imgLink" style="background-image:url('<?php echo asset_url(); ?>/img/site/upload.jpg')">
                                <h3 class="tac cf3">Upload Tài Liệu</h3>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="itemHolder textHolder">
                        <h2 class="mb15 fwb cf1 fs32"><i class="fa fa-paper-plane"></i>&nbsp;Thư Viện Phong Phú</h2>
                        <p>Thư viện đề thi, tài liệu chuyên ngànhm sách, khóa luận phong phú đầy đủ và cập nhật không ngừng.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="itemHolder textHolder">
                        <h3 class="mb15 fwb cf1 fs32"><i class="fa fa-cloud"></i>&nbsp;Tài Liệu &nbsp; Của Tôi</h3>
                        <p>Cung cập cho bạn một kho tài liệu cá nhân online để bạn có thể đọc, chia sẻ và tải xuống mọi lúc mọi nơi.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="itemHolder textHolder">
                        <h2 class="mb15 fwb cf1 fs32">
                        <i class="fa fa-crosshairs"></i>&nbsp;Các Trường Thành Viên
                        </h2>
                        <p>Với mỗi trường thành viên chúng toi cung cập một trang tài liệu riêng, ngoài ra còn có các thông tin hỗ trợ học tập khác.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="itemHolder textHolder">
                        <h2 class="mb15 fwb cf1 fs32">
                        <i class="fa fa-eye"></i>&nbsp;Hoàn Toàn Miễn Phí
                        </h2>
                        <p>Tất cả các tài liệu được cộng đồng chia sẻ đề hoàn toàn miễn phí. bạn chỉ cần upload lên một tài liệu để có thể download.</p>
                    </div>
                </div>
                <div class="item width3 height2">
                    <div class="itemHolder">
                        <style>

                        </style>
                        <div id='menu_tab' class="col-md-3">
                            <!-- required for floating -->
                            <!-- Nav tabs -->
                           <ul class="nav nav-pills nav-stacked">
                                <?php $class = 'active' ?>  
                                <?php foreach($hdr_groups as $group): ?> 
                                <li class="<?php echo $class; ?>" >
                                    <a href="<?php echo '#tab'.$group->group_id; ?>" data-toggle="tab"><i class="fa fa-briefcase"></i>&nbsp;&nbsp;<?php echo "Nhóm ".$group->group_name; ?></a>
                                </li>
                                <?php $class = ''; ?>
                                <?php endforeach; ?>  
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <?php $class = 'active' ?>
                                <?php foreach($hdr_groups as $group): ?>
                                    <?php echo "<div class='tab-pane $class' id='tab$group->group_id'>";
                                     $class = "";
                                    ?>
                                    <?php foreach($hdr_majors as $major): ?>
                                            <?php if($major->major_group == $group->group_id): ?>
                                                <a href="#"><?php echo $major->major_name; ?></a></br>
                                            <?php endif; ?>    
                                        <?php endforeach; ?>
                                    <?php echo "</div>";?>    
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item careers">
                    <div class="itemHolder textHolder">
                        <h3 class="mb15 fwb cf1 fs20"><i class="fa fa-area-chart"></i> Thống kê thành viên</h3>
                        <p>Chào mừng: <?php echo $newUser;?></p>
                        <p>Tổng số: <?php echo $totalUser;?></p>
                        <p>Lượt truy cập:</p>
                    </div>
                </div>
                <div class="item">
                    <div class="itemHolder textHolder">
                        <h3 class="mb15 fwb cf1 fs20"><i class="fa fa-area-chart"></i> Thống kê tài liệu</h3>
                        <p>Tài liệu mới: <?php echo "<a href= '".base_url()."index.php/doc/".$newDoc->doc_id."'>".$newDoc->doc_tittle."</a>";?></p>
                        <p>Tổng số: <?php echo $totalDoc;?></p>
                        <p>Lượt truy cập:</p>
                    </div>
                </div>
                <div class="item width4" style="height: 70px">
                    <div class="itemHolder" style="height: 60px; padding: 0.5%">
                        <h3 class="mb15 fwb cf1 fs30">Các Tài Liệu Nổi bật</h3>
                    </div>
                </div>
                <?php foreach($hotDocs as $hotDoc): ?>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <?php echo "<a href= '".base_url()."index.php/doc/".$hotDoc->doc_id."'>";?>
                            <div class="fullS imgLink" style="background-image:url('<?php echo asset_url(); ?>/img/home/doc.jpg')"></div>
                            <h3 class="cf3 fs17"><?php echo $hotDoc->doc_tittle;?></h3></a>
                    </div>
                </div>
                <?php endforeach;?>
                <?php for($i=4-sizeof($hotDocs)%4; $i>0; $i--): ?>
                    <div class="item lab">
                        <div class="itemHolder imgHolder fancyHover">
                            <a href="#">
                                <div class="fullS imgLink" style="background-image:url('<?php echo asset_url(); ?>/img/docview/empty_doc.png')"></div>
                                <h3 class="cf3 fs17">Empty document</h3></a>
                        </div>
                    </div>    
                <?php endfor;?>
                <div class="item width4" style="height: 70px">
                    <div class="itemHolder" style="height: 60px; padding: 0.5%">
                        <h3 class="mb15 fwb cf1 fs30">Sách Hay Trong ngày</h3>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/unique-culture">
                            <div class="fullS imgLink" style="background-image:url('<?php echo asset_url(); ?>/img/home/book.jpg')"></div>
                            <h3 class="cf3 fs17">Our Unique Culture</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/awesome-office.html">
                            <div class="fullS imgLink" style="background-image:url('<?php echo asset_url(); ?>/img/home/book.jpg')"></div>
                            <h3 class="cf3 fs17">Our Awesome Office</h3></a>
                    </div>
                </div>
                <div class="item">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="events">
                            <div class="fullS imgLink" style="background-image:url('<?php echo asset_url(); ?>/img/home/book.jpg')"></div>
                            <h3 class="cf3 fs17">Discover our great events & Join with us!</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder imgHolder fancyHover">
                        <a href="home/best-moments.html">
                            <div class="fullS imgLink" style="background-image:url('<?php echo asset_url(); ?>/img/home/book.jpg')"></div>
                            <h3 class="cf3 fs17">The best moments of SSS</h3></a>
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder textHolder" style="background-color: #FFF;">
                        <h2 class="cf1 fs50 mb15">D.S Các Trường ➤</h2>
                        <p>tất cả 69 website đơn vị</p>
                    </div>
                </div>
                <div class="item width3 lab">
                    <div class="itemHolder" style="background-color: #FFF;">
                        <ul class="link_list mb15 fwb cf1 fs15">
                            <?php foreach($hdr_schools as $school): ?>
                                <li>
                                <a href="<?php echo $school->school_name_short; ?>">
                                    <img src="<?php echo  base_url().$school->school_logo; ?>" />
                                    <div class="img_link_label"><?php echo $school->school_name_short; ?></div>
                                </a>
                            </li>    
                            <?php endforeach; ?>    
                        </ul>
                    </div>
                </div>
                <div class="item width3 lab">
                    <div class="itemHolder textHolder" style="background-color: #FFF;">
                        abc
                    </div>
                </div>
                <div class="item lab">
                    <div class="itemHolder textHolder rtl" style="background-color: #FFF;">
                        <h2 class="cf1 fs50 mb15">Top
                        <br>
                        T.Kiếm
                        <span class="rotate180">&#10148;</span>
                        </h2>
                        <p>top tìm kiếm nhiều nhất</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'footer.php' ?>
<?php include 'template_end.php' ?>
