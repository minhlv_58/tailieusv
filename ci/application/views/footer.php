    <main style='padding-top:10px'>
        <div class="container">
            <div class="isotope js-isotope">
                <div id="footer-contact-info" class="item width2 footer-contact-info">
                    <div class="itemHolder textHolder">
                        <address class="clearfix">
                            <p>
                                <span class="icoHolder"><i class="ico icoLogoGray"></i></span>
                                <strong class="ttu text mt4">NHÓM PHÁT TRỈÊN</strong>
                            </p>
                            <p>
                                <span class="icoHolder"><i class="ico icoAddr"></i></span>
                                <span class="text"><b>Team ETC: </b> Đại Học Công Nghệ - Đại Học Quốc Gia Hà Nội</span>
                            </p>
                            <p>
                                <span class="text"><b>Đia chỉ:</b> 144 Xuân Thủy, Hà Nội, Việt Nam</span>
                            </p>
                            <p style="margin-bottom: 0;">
                                <span class="icoHolder"><i class="ico icoMail"></i></span>
                                <span class="text">etcuet@gmail.com hoặc etcuet@gmail.com</span>
                            </p>
                            <p style="margin-bottom: 0;">
                                <span class="icoHolder"><i class="ico icoPhone"></i></span>
                                <span class="text">+84 8 6271 8255</span>
                            </p>
                        </address>
                    </div>
                </div>
                <div id="footer-contact-form" class="item width2 footer-contact-form">
                    <div class="itemHolder textHolder contactFormHolder">
                        <h3 class="mb12 fwb cf1 fs28 col-md-12"> Yêu cầu tài liệu
                        
                        </h3>
                        <a href="#" title="Header" data-toggle="popover" data-trigger="hover" data-content="Some content">Hover over me</a>
                        <form action="http://www.siliconstraits.vn/contact" method="POST" role="form" id='contactForm' class='contactForm'>
                            <input type="hidden" name="csrf_token" value="ba2b7962ddfb805eb181bfd128b9276a" />
                            <div class="form-group clearfix">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" minlength="2" id="name" name="name" placeholder="Name" value="">
                                </div>
                                <div class="col-md-6">
                                    <input type="email" class="form-control " id="email" name="email" placeholder="Email" value="">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone number" value="">
                                </div>
                                <div class="col-md-6">
                                    <input type="url" class="form-control " id="website" name="website" placeholder="Website" value="">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="2" class="form-control " placeholder="Your message"></textarea>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3">
                                    <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2ODApLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAKACWAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8AioorS8PFBqLFkt5ZVt7hreO7K+S1wIXMIfcyjaZNgOWAxnmmld2Gld2M2iqdle/HXRtJsm8SfB/w38RYmiXUrvU/CU8ccllA2Ea2/wBEkWJpU8iV+VkH71fm7U/wJ4/+HXxovL6DwL4kNrqUc0KwaH4rnsNPu7kS79q2xF3Is7LswyrhhuX5eRRa+nXzDd26+ZZp0cbyuFRWdj0VRkmr+veHdU8L35stX0+5027A3eTdRFGIyRkZ6jIPI44qhq+oyaF8MfiPq8FxNZ3dloLLbXVu22SCWe5t7VXU9iPtBINJtRu56JCdo3ctLGrpGhie5uX1JpdP0+xtpL28nMZzFDGpZmwfp3rz6b9ozTFtFa08F+HtBtriWM2lx4l12Rrl0BEm2SPYsYZ48cBwVDjrxXUfD7w/8ZfiX4Ph0a1+I0dp4L1Se501pPFmsRzXWsPIdk0QZYZp1jG4IFLRLyDjnNXP2XPiDe694N8V+CdFvW0fxbJrs+pS+GL6NQt9byWVrBNbmOUbZHiktpVaNl+6wODmp9o5uMKd0pX1aV36f8DXUnncpQjT0v3Sd/S90/18xfCevQfE6+h0nR/DGmQeL54nnh8Pp4juLSS9RQCxtDPYyQSkAk7BdbsKxxxXW23wl8ffYLu51DwDp3g2C3UPJqXjLxNIlpbpn5pJPIsdm0Dnm4T6iuS+Nf7O5uvhRfeLbP4c2PgvXvD86STrptkNKttRtHBjdhDHsjEsRw4eNQxGRzWTpPwY+HPjPTPA+veGPgzYaFcXwVNW8Q+M9Sur5ZJ4zEs9tYWVxdStO+5pF82eBIkAyC2RWlP2inyT1000WvS/w3uvVFcs5S5ZLXR3V1f5bLXfRLtYvaT418BeLbBZ/Dus+IfiGwf9/P8ADv4baq1pa/Kg2P8AarlnJBDHcDzu+6KhtvFfw41nVrvS9L+JuhW+rWiBp9K8Uxz+H7qNuP3Z+2pHEX5HyrKT36c12WsfHHT9d1S4e7+Pnhy0maZsWZ1+aOOA7iNoIURKBjHBxVubxJq/i/R5bafxD4U+LHh2CZXktLvUtP8AEdokgBK5jd5fLbGccKcZx3rVpPVbv+ujNVCKSS1/r1Od8SeDtb8ITRR6xplxYecCYnlX93KAAco4+VxgjlSeo9ax66jwJ4Q0a1u7Lw/pfhHTvCugyalPqdxaaeboxPPLHGjth5XKqRbxgLHtA5wBXE+GPFGtXfgNdU+JXhSPwP4in1HNtommWbxk2P2eEh2WaZ3Vi5k4kYHjgdgV6awsfaYhqnF9Ze6vvdvu3fRFYmmsJH2mJapxfWb5Vftd2u/Javoi9RT3bVlQgeGI7fIwr3V4Dgjnkbl6gH0600y6mFPmeFZyEUF5LadmVs9CmFbI45ALYzWLtH4lJLv7Kt/8rvtr2t1MLxWslNLu6Nb7v4d9tdrW67iUVJaSXks6C40VdPstrZluJf3pIBOQGKkgnA4Tv1qOjePMr281KN9ndKSTtro7Weo1Zx5le1+sZRvondKSTs76O1nr2CiiikIKKKKAJrO9uNOuorm0nltbmJt0c0LlHQ+oI5Bq348l0L4z6daWXxO0N/GJsUmSy1L+0JrS/thIEDYlQlZB+7UhZkkA545IqjDDJcPsijaR+u1ASa2NU8H3/hyFJ/EUlh4Stn24n8S6hBpaYLFQc3DpnkEcelUrtW6FJNq3Q5rw94P8d/DiwSx+EnxTsbvwjpqG7sPh349to5Flfb5s1uZ1gjhxJO8xBDQcMuTxkR+LNU+Kt34H1iDxjafCeLwnr8lnp97D4Xkge60wxziWNcx3IJMsixjl5/8AU8KPmrRsPEGiSzqvhs6l8StVDIi2ngy0eayhleNGjFzqcirawpukRHdXk8s53AYNS3egQ+I/B95onirVJPCljd3VteQ+HfAkdnP9nEbPLH9s1C4ikluLlPM2FoXWIbCQpycudOpKKjFrl1unfysl011Tu9N7MJxqNKMWuXW6d/K1rO2uqd3po+Vlb4c+JPiifCWmWvgz4WaB448I6TcCDT557SazkvJLiaeWW4iu5LzyrgRGzkjfYUAMyDBHA53xcvhDxr4ij0L9oLSPEngT4l6iIxPqsGj6HaaHFGWIhunl3tLJuUBXkkkkVWzyoAA7C+ubf+z9E0uya8bTdF06HTLM6hMss5ijz80jKiAsWZmOFHJNS6pBY+OdL0Ww1nxN4p0CbRFnGnXGk3Uc1mpkx/r7SWNhIBzgqyEAnnpU8t6ap817W66adk9F/wABXE43gqfM7q3V207J6Lf52V7nLWf7Nfh3w/4a8Q/EK48R3HjDw/o+lXN7bXdlHY3Ucs8UTrbxi9S9uELFxsCJGpzxxVz4R3niDTPhRpl7bXdnp8KeK9Yms5ZFzK6AWqsyny3G0TLOMjHKn0rDufhZonhnxELfXviJo+k6FqdvbajNq1j4M1aeC8u45bhIIpLaGYRySonnNuYkqG4B7bXhWb4qW3g+w0fSPij8AhoEVjC8F5eLbpPoiqrSMsUMunRv5lwZJJGSaGZt0TZZXJLRDBV6UFPlnyvaSsrvTRSgo6eSd09GzjhB0/d5pX7qUYy++moNJ+t/Owt/4T+DPwJ8Ap4k8UeEJ9WFwWTSdMuvEF+JNSnHMkoTzdkcQYks+3qcAEnFTfBD4KP4m+J3g3xz4y0PSfCNhLcXIsPDqRSS6hex3UM0YLlm3QwoJVK72LNt6DisDQNS8WeNde0T4lTeDLz4h+IYrG2S2OoeC7vULC1WOCMAxWdq0C8MS+VXbufIUcY7jwz+1D4X0n4x6QfjLo198N9eS0W6l1J49QK3V2pjUA6bNYrPCsgaRlZZJFUqBk5yM6f1erK9RLk2s9U115m7t9rN7b31NoewqS/epcm1nqmuvM3e/o3tvfU858AePNavtP1W3i+FMnxe1Gz1N4rGXRdNu4vsdqk06Kt39iiKuzBUYFihIznPU9j8L9Q8S3cnxntdf1C4fU9Fu9KKWRGxNMWWW9SS1jHUKjRqvJJ+UEkknK/sweG/E2n/ALM+r3A03UXabxhHcXHlxSFRt061zuGOB5pbqBz71b0LRrnT/wBrH9orQbWGR4tT0cauIVUlmdZNNnyBj11CX9aeEw8cM4V6MVFvT3YqL1T0ukm7W6iweGp4Rwr0IKDenuxjF6p6XSTa8rlZmLsWYksTkk9TSU+e3ltZTFNE8MgwSkilSMjI4PsaZWz8zd+YUUUUhBRRRQAVYimsrA2L3lnqmsXF9NJDZaLoUcT3t60UfmzFfMdFjjRCC8rEhdy/K2cUUVS7+n5pDXf0/NI1das5bbWHj1DxXc2ulwSqB4Y8B3sltbybNqSLdasYYrm5SQI5KxRwhfM+WQ4zWRFD4fsbia507wT4VsbybeZb99Iivb2RnUrI7Xd0JZ2dwzFnMhYliSeaKKuc3do0lJ3Zpa/4y13xTKZNX1i91JiScXM7Ooyc8AnAGewrHoorNtvczbb3CiiikIsQ30kUPkskFzblg/kXdvHPFuAIB2OCM4ZhnHc1HFa6BHqUWoHwtpv2yOaa4DwS3NupeRgzFkimRWAIOFIIXJwBRRSjeEuanJxfeMnF9t4tPYfqM1LTNA1uzS2vNP1KNESKJHtdbunZVQnI23LTod2cEsh4Axiuj0Dxh8QPCljcaP4Y1yP4g+Fr+NrKXwf4yY7oLUAKkEEpuVR9yM65CoBsTCGiiljc2xuEw06sqsqijry1JSmn/wCBNyWqTvCUX52bTqMVOS6Puv6/M4jTvh58HNJ0ybxFo2vfF34I69bzQWt5c2bajPqRspWVFti0cQXymfyGUmRj+7HynjNfwh4R1Xx1rHiXxZN49+KXhPw5ZSRadZ+LLuCS113W7ZmANokv2kL8kdrbF5fmDEAtGGYiiivVx+X0sHUoQg21OHPr0bctFZLRWVr3fds54tTnGDWiTfzVzTPhn4haTbzWXgT9oe7vtOnufLTSPirphvYYLdCRbCOR479A6q5V2SOANtBIGBjZtNZ/tHxdF4T8e+D9Q+Gfiu7ubPStI1mCK4vvD/ia7aQW8kls8dogt0eR4XXLMoEwDbSOSivPXux5vNL8zoXuq/ml+ZnUUUVBAUUUUAf/2Q==" height="30" />
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="captcha" placeholder="captcha">
                                </div>
                                <div class="col-md-5">
                                    <button type="submit" class="btn btn-link pull-right">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <div class="container clearfix">
            <br>
            <p class="tac copy">Chào mừng đến với tailieusv.com</p>
            <br>
        </div>
    </footer>