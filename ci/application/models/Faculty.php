<?php
class Faculty extends CI_Model {
    public $faculty_id;
    public $faculty_name;
    public $faculty_name_short;
    public $faculty_type;
    public $faculty_major;
    public $faculty_school;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function init($data){
    	foreach ($data as $key => $value) {
    		$this->$key = $value;
    	}
    }
    public function findById($id){
    	$sql = "SELECT * FROM `tbl_faculties` WHERE `faculty_id`=?";
    	$query = $this->db->query($sql, array($id));
    	$row = $query->result();
    	$this->init($doc);
    	return $this;
    }
    public function findBy($array){
    	$query = $this->db->get_where('tbl_faculties', $array);
    	return $query->result();
    }
    public function findByLimit($array, $limit, $offset){
        $query = $this->db->get_where('tbl_faculties', $array, $limit, $offset);
        return $query->result();
    }

    public function findAll(){
    	if(func_num_args() > 0){
            $limit = func_get_args(0);
            if(func_num_args() > 1){
                $offset = func_get_args(1);
            } else{
                $offset = 0;
            }
            $query = $this->db->get('tbl_faculties', $limit, $offset);
        } else {
            $query = $this->db->get('tbl_faculties');
        }
        return $query->result();
    }
    public function findWithWhere($array){
        if(func_num_args() > 0){
            $limit = func_get_args(0);
            if(func_num_args() > 1){
                $offset = func_get_args(1);
            } else{
                $offset = 0;
            }
            $query = $this->db->get_where('tbl_faculties', $array);
        } else {
            $query = $this->db->get_where('tbl_faculties', $array);
        }
        return $query->result();
    }

    public function insert(){
    	$this->doc_id = $this->db->insert_id()+1;
    	$this->db->insert('tbl_faculties', $this);
    }

    public function update($id){
    	$this->db->update('tbl_faculties', $this, array('faculty_id' => $this->faculty_id));
    }

    public function delete($array){
    	$this->db->delete('tbl_faculties',$array);
    }
}
?>