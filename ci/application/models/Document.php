<?php
class Document extends CI_Model {
	public $doc_id;
	public $doc_tittle;
	public $doc_thubnal;
	public $doc_desc;
	public $doc_text;
	public $doc_author;
	public $doc_file_path;
	public $doc_file_type;
	public $doc_file_size;
	public $doc_file_unit;
	public $doc_type;
	public $doc_status;
	public $doc_up_by;
	public $doc_catalog;
	public $doc_up_on;
	public $doc_hash_md5;
	public $doc_hash_sha1;
	public $doc_down_total;
	public $doc_down_last;
	public $doc_vote_total;
	public $doc_vote_avg;
	public $doc_report_total;
	public $doc_view_total;
	public $doc_share_total;
	public $doc_cost;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function init($data){
    	foreach ($data as $key => $value) {
    		$this->$key = $value;
    	}
    }
    public function findById($id){
    	$sql = "SELECT * FROM `tbl_documents` WHERE `doc_id`=?";
    	$query = $this->db->query($sql, array($id));
    	return $query->result();
        }
    public function find($array, $limit, $offset){
    	$query = $this->db->get_where('tbl_documents', $array, $limit, $offset);
    	return $query->result();
    }

    public function findAll($limit, $offset){
        $query = $this->db->get('tbl_documents', $limit, $offset);
        return $query->result();
    }

    public function findHotDocument(){
        $this->db->select('*');
        $this->db->from('tbl_documents');
        $this->db->order_by('doc_view_total','asc'); //desc giam dan, asc tang dan
        $this->db->limit(8,0); //limit/offset
        $query = $this->db->get();
        return $query->result();
    }

    public function totalDoc(){
        return $this->db->count_all_results('tbl_documents');;
    }

    public function findLastDoc(){
        $sql = "SELECT * FROM `tbl_documents` ORDER BY `doc_id` DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function insert(){
    	$this->doc_id = $this->db->insert_id()+1;
    	$this->db->insert('tbl_documents', $this);
    }

    public function update($id){
    	$this->db->update('tbl_documents', $this, array('doc_id' => $this->doc_id));
    }

    public function delete($array){
    	$this->db->delete('tbl_documents',$array);
    }

    public function vote($val){
    	$this->doc_vote_avg = ($this->doc_vote_total*$this->doc_vote_avg + $val)/($doc_vote_total + 1);
    	$this->doc_vote_total++;
    	return $doc_vote_avg;
    }

    public function download(){
    	$this->doc_down_last = date('d-m-y');
    	$this->doc_down_total++;
    }

    public function view(){
    	$this->doc_view_total++;
    }

    public function share(){
    	$this->doc_share_total++;
    }
}
?>