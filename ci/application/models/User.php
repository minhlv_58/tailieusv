<?php
class User extends CI_Model {
    public $user_id;
    public $user_name;
    public $user_email;
    public $user_pass;
    public $user_salt;
    public $user_type;
    public $user_status;
    public $user_balance;
    public $user_up_total;
    public $user_last;
    public $user_avatar;
    public $user_school;
    public $user_faculty;
    public $user_major;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function init($data){
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }
    public function findById($id){
        $sql = "SELECT * FROM `tbl_users` WHERE `user_id`=?";
        $query = $this->db->query($sql, array($id));
        return $query->result();
    }
    public function find($array, $limit, $offset){
        $query = $this->db->get_where('tbl_users', $array, $limit, $offset);
        return $query->result();
    }

    public function findAll($limit, $offset){
        if(func_num_args() > 0){
            $limit = func_get_args(0);
        if(func_num_args() > 1){
            $offset = func_get_args(1);
        } else{
            $offset = 0;
        }
        $query = $this->db->get('tbl_users', $limit, $offset);
        } else {
            $query = $this->db->get('tbl_users');
        }
        return $query->result();
    }

    public function findLastUser(){
        $sql = "SELECT * FROM `tbl_users` ORDER BY `user_id` DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function totalUser(){
        return $this->db->count_all_results('tbl_users');;
    }

    public function insert(){
        $this->doc_id = $this->db->insert_id()+1;
        $this->db->insert('tbl_users', $this);
    }

    public function update($id){
        $this->db->update('tbl_users', $this, array('user_id' => $this->user_id));
    }

    public function delete($array){
        $this->db->delete('tbl_users',$array);
    }
}
?>