<?php
class Subject extends CI_Model {
    public $tag_id;
    public $tag_name;
    public $tag_type;
    public $tag_parent;
    public $tag_status;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function init($data){
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }
    public function findById($id){
        $sql = "SELECT * FROM `tbl_tags` WHERE `tag_id`=?";
        $query = $this->db->query($sql, array(id));
        $row = $query->array_result();
        $this->init($doc);
        return $this;
    }
    public function find($array, $limit, $offset){
        $query = $this->db->get_where('tbl_tags', $array, $limit, $offset);
        return $query->array_result();
    }

    public function findAll($limit, $offset){
        $query = $this->db->get('tbl_tags', $limit, $offset);
        return $query->array_result();
    }
    public function subjectByFaculty($faid){
        $sql = "SELECT * FROM `tbl_subjects` WHERE `subject_id` IN (SELECT `subject_id` FROM `tbl_faculties-subjects` WHERE `faculty_id`= ? ) ORDER BY `subject_name`";
        $query = $this->db->query($sql, array($faid));
        return $query->result();
    }

    public function insert(){
        $this->doc_id = $this->db->insert_id()+1;
        $this->db->insert('tbl_tags', $this);
    }

    public function update($id){
        $this->db->update('tbl_tags', $this, array('tag_id' => $this->tag_id));
    }

    public function delete($array){
        $this->db->delete('tbl_tags',$array);
    }
}
?>