<?php
class School extends CI_Model {
    public $school_id;
    public $school_name;
    public $school_name_short;
    public $school_type;
    public $school_status;
    public $school_logo;
    public $school_desc;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function init($data){
    	foreach ($data as $key => $value) {
    		$this->$key = $value;
    	}
    }
    public function findById($id){
    	$sql = "SELECT * FROM `tbl_schools` WHERE `school_id`=?";
    	$query = $this->db->query($sql, array(id));
    	$row = $query->result();
    	$this->init($doc);
    	return $this;
    }
    public function find($array, $limit, $offset){
    	$query = $this->db->get_where('tbl_schools', $array, $limit, $offset);
    	return $query->result();
    }

    public function findAll(){
        if(func_num_args() > 0){
            $limit = func_get_args(0);
            if(func_num_args() > 1){
                $offset = func_get_args(1);
            } else{
                $offset = 0;
            }
            $query = $this->db->get('tbl_schools', $limit, $offset);
        } else {
            $query = $this->db->get('tbl_schools');
        }
        return $query->result();
    }

    public function insert(){
    	$this->doc_id = $this->db->insert_id()+1;
    	$this->db->insert('tbl_schools', $this);
    }

    public function update($id){
    	$this->db->update('tbl_schools', $this, array('school_id' => $this->school_id));
    }

    public function delete($array){
    	$this->db->delete('tbl_schools',$array);
    }
}
?>