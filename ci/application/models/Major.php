<?php
class Major extends CI_Model {
    public $major_id;
    public $major_name;
    public $major_name_short;
    public $major_group;
    public $major_desc;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function init($data){
    	foreach ($data as $key => $value) {
    		$this->$key = $value;
    	}
    }
    public function findById($id){
    	$sql = "SELECT * FROM `tbl_majors` WHERE `major_id`=?";
    	$query = $this->db->query($sql, array(id));
    	$row = $query->array_result();
    	$this->init($doc);
    	return $this;
    }
    public function findByGroup($grid){
       $query = $this->db->get_where('tbl_majors', array('major_group' => $grid));
       return $query->result();
    }
    public function find($array, $limit, $offset){
        if(func_num_args() > 1){
            $limit = func_get_args(1);
            if(func_num_args() > 2){
                $offset = func_get_args(2);
            } else{
                $offset = 0;
            }
            $query = $this->db->get_where('tbl_majors', $array, $limit, $offset);
        } else {
            $query = $this->db->get_where('tbl_majors', $array);
        }
        return $query->result();
    }

    public function findAll(){
        if(func_num_args() > 0){
            $limit = func_get_args(0);
            if(func_num_args() > 1){
                $offset = func_get_args(1);
            } else{
                $offset = 0;
            }
            $query = $this->db->get('tbl_majors', $limit, $offset);
        } else {
            $query = $this->db->get('tbl_majors');
        }
        return $query->result();
    }

    public function insert(){
    	$this->doc_id = $this->db->insert_id()+1;
    	$this->db->insert('tbl_majors', $this);
    }

    public function update($id){
    	$this->db->update('tbl_majors', $this, array('major_id' => $this->major_id));
    }

    public function delete($array){
    	$this->db->delete('tbl_majors',$array);
    }
}
?>