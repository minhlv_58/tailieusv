<?php
class Group extends CI_Model {
    public $group_id;
    public $group_name;
    public $group_name_short;
    public $group_desc;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function init($data){
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }
    public function findById($id){
        $sql = "SELECT * FROM `tbl_groups` WHERE `group_id`=?";
        $query = $this->db->query($sql, array(id));
        $row = $query->array_result();
        $this->init($doc);
        return $this;
    }
    public function find($array, $limit, $offset){
        $query = $this->db->get_where('tbl_groups', $array, $limit, $offset);
        return $query->array_result();
    }

    public function findAll(){
        if(func_num_args() > 0){
            $limit = func_get_args(0);
            if(func_num_args() > 1){
                $offset = func_get_args(1);
            } else{
                $offset = 0;
            }
            $query = $this->db->get('tbl_groups', $limit, $offset);
        } else {
            $query = $this->db->get('tbl_groups');
        }
        return $query->result();
    }

    public function insert(){
        $this->doc_id = $this->db->insert_id()+1;
        $this->db->insert('tbl_groups', $this);
    }

    public function update($id){
        $this->db->update('tbl_groups', $this, array('group_id' => $this->group_id));
    }

    public function delete($array){
        $this->db->delete('tbl_groups',$array);
    }
}
?>