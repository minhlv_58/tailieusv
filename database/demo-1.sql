/*minhlv*/
/*đây là file sql sinh databse, cách cài đặt: vào phpadmin -> chọn data -> chọn SQL -> paste script*/

CREATE TABLE IF NOT EXISTS `tbl_groups`(
	`group_id` INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`group_name` VARCHAR(300) NOT NULL,
	`group_name_short` VARCHAR(10),
	`group_desc` TEXT
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_majors`(
	`major_id` INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`major_name` VARCHAR(300) NOT NULL,
	`major_name_short` VARCHAR(10),
	`major_desc` TEXT,
	`major_group` INT(8) UNSIGNED, /*foreign key*/
	FOREIGN KEY (major_group) REFERENCES tbl_major_groups(group_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_schools`(
	`school_id` INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`school_name` VARCHAR(300) NOT NULL,
	`school_name_short` VARCHAR(10),
	`school_type` TINYINT,
	`school_status` TINYINT,
	`school_logo` VARCHAR(300) NOT NULL,
	`school_desc` TEXT
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_faculties`(
	`faculty_id` INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`faculty_name` VARCHAR(300) NOT NULL,
	`faculty_name_short` VARCHAR(10),
	`faculty_type` TINYINT,
	`faculty_desc` TEXT,
	`faculty_school` INT(8) UNSIGNED, /*foreign key*/
	`faculty_major` INT(8) UNSIGNED, /*foreign key*/
	FOREIGN KEY (faculty_school) REFERENCES tbl_schools(school_id) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (faculty_major) REFERENCES tbl_majors(major_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_users`(
	`user_id` INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`user_name` VARCHAR(300) NOT NULL,
	`user_email` VARCHAR(300) NOT NULL,
	`user_pass` VARCHAR(32) NOT NULL,
	`user_salt` VARCHAR(32) NOT NULL,
	`user_type` TINYINT,
	`user_status` TINYINT,
	`user_balance` INT NOT NULL,
	`user_up_total` INT,
	`user_up_last` DATETIME,
	`user_avatar` VARCHAR(300),
	`user_school` INT(8) UNSIGNED , /*foreign key*/
	`user_faculty` INT(8) UNSIGNED,  /*foreign key*/
	`user_major` INT(8) UNSIGNED,  /*foreign key*/
	FOREIGN KEY (user_school) REFERENCES tbl_schools(school_id) ON UPDATE CASCADE,
	FOREIGN KEY (user_major) REFERENCES tbl_majors(major_id) ON UPDATE CASCADE,
	FOREIGN KEY (user_faculty) REFERENCES tbl_faculties(faculty_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_subjects`(
	`subject_id` INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`subject_name` VARCHAR(300) NOT NULL,
	`subject_type` TINYINT,
	`subject_status` TINYINT,
	`subject_desc` TEXT
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_documents`(
	`doc_id` INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`doc_tittle` VARCHAR(300) NOT NULL,
	`doc_thubnail` VARCHAR(300),
	`doc_desc` TEXT,
	`doc_text` TEXT,
	`doc_author` VARCHAR(300),
	`doc_file_path` VARCHAR(300) NOT NULL,
	`doc_file_type` VARCHAR(10),
	`doc_file_size` FLOAT(10,2),
	`doc_file_unit` VARCHAR(10) DEFAULT 'kb',
	`doc_type` TINYINT,
	`doc_status` TINYINT NOT NULL,
	`doc_up_by` INT(8) UNSIGNED, /*foreign key*/
	`doc_catalog` INT(8) UNSIGNED, /*foreign key*/
	`doc_up_on` DATETIME,
	`doc_hash_md5` VARCHAR(32),
	`doc_hash_sha1` VARCHAR(40),
	`doc_down_total` INT(8) NOT NULL DEFAULT 0,
	`doc_down_last` DATETIME,
	`doc_vote_total` INT(8) NOT NULL,
	`doc_vote_avg` FLOAT(4,1),
	`doc_report_total` INT(8)  DEFAULT 0,
	`doc_view_total` INT(8) DEFAULT 0,
	`doc_share_total` INT(8) DEFAULT 0,
	`doc_cost` INT(6),
	FOREIGN KEY (doc_up_by) REFERENCES tbl_users(user_id) ON UPDATE CASCADE,
	FOREIGN KEY (doc_catalog) REFERENCES tbl_majors(major_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_tags`(
	`tag_id` INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`tag_name` VARCHAR(100) NOT NULL,
	`tag_type` TINYINT,
	`tag_parent` INT(8) UNSIGNED, /*foreign key*/
	`tag_status` TINYINT,
	FOREIGN KEY (tag_parent) REFERENCES tbl_tags(tag_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_docs-tags`(
	`doc_id` INT(8) UNSIGNED NOT NULL,
	`tag_id` INT(8) UNSIGNED NOT NULL,
	PRIMARY KEY (doc_id, tag_id),
	FOREIGN KEY (doc_id) REFERENCES tbl_documents(doc_id) ON UPDATE CASCADE,
	FOREIGN KEY (tag_id) REFERENCES tbl_tags(tag_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_faculties-subjects`(
	`faculty_id` INT(8) UNSIGNED NOT NULL,
	`subject_id` INT(8) UNSIGNED NOT NULL,
	`rela_called` VARCHA(300),
	PRIMARY KEY (faculty_id, subject_id),
	FOREIGN KEY (faculty_id) REFERENCES tbl_faculties(faculty_id) ON UPDATE CASCADE,
	FOREIGN KEY (subject_id) REFERENCES tbl_subjects(subject_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_majors-subjects`(
	`major_id` INT(8) UNSIGNED NOT NULL,
	`subject_id` INT(8) UNSIGNED NOT NULL,
	PRIMARY KEY (major_id, subject_id),
	FOREIGN KEY (faculty_id) REFERENCES tbl_majors(major_id) ON UPDATE CASCADE,
	FOREIGN KEY (subject_id) REFERENCES tbl_subjects(subject_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_faculties-docs`(
	`doc_id` INT(8) UNSIGNED NOT NULL,
	`faculty_id` INT(8) UNSIGNED NOT NULL,
	PRIMARY KEY (doc_id, faculty_id),
	FOREIGN KEY (doc_id) REFERENCES tbl_documents(doc_id) ON UPDATE CASCADE,
	FOREIGN KEY (faculty_id) REFERENCES tbl_faculties(faculty_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_subjects-docs`(
	`doc_id` INT(8) UNSIGNED NOT NULL,
	`subject_id` INT(8) UNSIGNED NOT NULL,
	PRIMARY KEY (doc_id, subject_id),
	FOREIGN KEY (doc_id) REFERENCES tbl_documents(doc_id) ON UPDATE CASCADE,
	FOREIGN KEY (subject_id) REFERENCES tbl_subjects(subject_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_majors-docs`(
	`doc_id` INT(8) UNSIGNED NOT NULL,
	`major_id` INT(8) UNSIGNED NOT NULL,
	PRIMARY KEY (doc_id, major_id),
	FOREIGN KEY (doc_id) REFERENCES tbl_documents(doc_id) ON UPDATE CASCADE,
	FOREIGN KEY (major_id) REFERENCES tbl_majors(major_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_users-subjects`(
	`user_id` INT(8) UNSIGNED NOT NULL,
	`subject_id` INT(8) UNSIGNED NOT NULL,
	PRIMARY KEY (user_id, subject_id),
	FOREIGN KEY (user_id) REFERENCES tbl_users(user_id) ON UPDATE CASCADE,
	FOREIGN KEY (subject_id) REFERENCES tbl_subjects(subject_id) ON UPDATE CASCADE
) ENGINE = innoDB;

CREATE TABLE IF NOT EXISTS `tbl_users-docs`(
	`user_id` INT(8) UNSIGNED NOT NULL,
	`doc_id` INT(8) UNSIGNED NOT NULL,
	PRIMARY KEY (user_id, doc_id),
	FOREIGN KEY (user_id) REFERENCES tbl_users(user_id) ON UPDATE CASCADE,
	FOREIGN KEY (doc_id) REFERENCES tbl_documents(doc_id) ON UPDATE CASCADE
) ENGINE = innoDB;