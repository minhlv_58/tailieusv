/*minhlv*/
/*các kịch bản truy suất dữ liệu*/
/*1. danh sách các trường*/
SELECT * FROM `tbl_schools` WHERE `school_status`=1 ORDER BY `school_name`;
/*2. danh sách khoa của trường id X*/
SELECT * FROM `tbl_faculties` WHERE `faculty_school`='X' AND `faculty_type`<>0 ORDER BY `faculty_name`;
/*3. danh sách môn của khoa X*/
SELECT * FROM `tbl_subjects` WHERE `subject_faculty`='X' AND `subject_status`=1 ORDER BY `subject_name`; 
/*4. danh sách danh sách tài liệu môn id X*/
SELECT * FROM `tbl_documents` WHERE `doc_id` IN (SELECT `doc_id` FROM `tbl_subjects-docs` WHERE `subject_id`='X') ORDER BY `doc_name`;
/*5. danh sách danh user*/
SELECT * FROM `tbl_users` WHERE 'user_type'=1 ORDER BY `user_name`;
/*5. danh sách tài liệu do user id x up*/
SELECT * FROM `tbl_documents` WHERE `doc_up_by`=x ORDER BY `doc_up_on`;
/*6. danh sách tài liệu my document*/
SELECT * FROM `tbl_documents` WHERE `doc_id` IN (SELECT `doc_id` FROM `tbl_users-docs` WHERE `user_id`='X') ORDER BY `doc_name`;
/*6. tổng số tài liệu môn x*/
SELECT COUNT(`doc_id`) FROM `tbl_subjects-docs` WHERE `subject_id`=x;
/*4. danh sách môn theo khoa*/
SELECT * FROM `tbl_subject` WHERE `subject_id` IN (SELECT `subject_id` FROM `tbl_faculties-subjects` WHERE `faculty_id`='X') ORDER BY `subject_id`;